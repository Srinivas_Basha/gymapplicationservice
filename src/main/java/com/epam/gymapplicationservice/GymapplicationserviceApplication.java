package com.epam.gymapplicationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymapplicationserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GymapplicationserviceApplication.class, args);
	}

}
