package com.epam.gymapplicationservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/gym")
public class GymController {

    @GetMapping("/test")
    public String test(){
        return "Endpoint works";
    }
}
